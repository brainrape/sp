{ pkgs ? import ./nixpkgs-pinned.nix {}, ... }:
let
  check = pkgs.writeShellScriptBin "check" ''
    set -e
    echo TODO
    echo "Success."
  '';
  build = pkgs.writeShellScriptBin "build" ''
    set -e
    elm make src/Main.elm --output public/main.js
    cp index.html public/
  '';
in
  pkgs.stdenv.mkDerivation rec {
    name = "morris-shell";
    src = ./.;
    buildInputs = [
      pkgs.elmPackages.elm
      pkgs.elmPackages.elm-format
      check
      build
    ];
  }
