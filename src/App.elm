module App exposing (Model, Msg(..), initModel, typAlias)

import Ctx exposing (..)
import Dict
import Editor.PureTree
import Elm.Docs
import Http
import Keyboard exposing (..)
import Structure.Edit
import Structure.Env
import Structure.PureTree
import Structure.Syntax exposing (Locus, Syntax)
import Structure.Tree
import Structure.Typ
import Structure.TypAlias
import Structure.TypEnv
import Time exposing (Posix)


typAlias : Structure.TypAlias.TypAlias
typAlias =
    { typId = ( "Main", "MyAlias" ), args = [ "a", "b" ], typ = Structure.Typ.ex }


syn : Syntax
syn =
    Structure.Syntax.SynTypAlias typAlias


type alias Model =
    { pressedKeys : List Key
    , keyChanges : List KeyChange
    , docs : List Elm.Docs.Module
    , env : Structure.Env.Env
    , typenv : Structure.TypEnv.TypEnv
    , ctx : Ctx
    , edit : Maybe Structure.Edit.Edit
    , time : Int
    , syn : Syntax
    , tree : Structure.Tree.Tree
    , pureTree : Structure.PureTree.Tree
    , pureTreeCtx : Editor.PureTree.Ctx
    }


initModel : Model
initModel =
    { pressedKeys = []
    , keyChanges = []
    , docs = []
    , env = Structure.Env.emptyEnv
    , typenv = Dict.empty
    , ctx = initCtx
    , edit = Nothing
    , time = 0
    , syn = syn
    , tree = Structure.Tree.toTreeSyntax syn
    , pureTree = Structure.PureTree.init
    , pureTreeCtx = Editor.PureTree.initCtx
    }


type Msg
    = GotDocs (Result Http.Error (List Elm.Docs.Module))
    | Pointer Locus
    | Cursor Locus
    | KeyMsg Keyboard.Msg
    | PureTreeMsg Editor.PureTree.Msg
    | Tick Posix
    | Blur
