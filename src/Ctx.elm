module Ctx exposing (Ctx, Look, initCtx, look, stepCtx)

import Structure.Env exposing (..)
import Structure.Syntax exposing (..)


type alias Look =
    { fullNames : Bool
    }


look : Look
look =
    { fullNames = False
    }


type alias Ctx =
    { here : Locus
    , cursor : Locus
    , pointer : Locus
    }


initCtx : Ctx
initCtx =
    { here = []
    , cursor = []
    , pointer = []
    }


stepCtx : Focus -> Ctx -> Ctx
stepCtx foc ctx =
    { ctx
        | here = ctx.here ++ [ foc ]
        , cursor = stepF foc ctx.cursor
        , pointer = stepF foc ctx.pointer
    }
