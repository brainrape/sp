module DocJsonToEnv exposing (Err, convertAlias, convertCase, convertTypDef, convertValue, docJsonToEnv, docJsonToTypEnv, modulToEnv, modulToTypEnv)

import Dict
import DocJson
import Structure.Env exposing (..)
import Structure.Typ exposing (..)
import Structure.TypAlias exposing (..)
import Structure.TypDef exposing (..)
import Structure.TypEnv exposing (..)
import Structure.Value exposing (..)


type alias Err =
    ()


docJsonToEnv : List DocJson.Modul -> Result Err (Env a)
docJsonToEnv moduls =
    Ok (moduls |> List.map modulToEnv |> List.foldr overrideEnv emptyEnv)


modulToEnv : DocJson.Modul -> Env a
modulToEnv modul =
    { aliases =
        modul.aliases
            |> List.map (convertAlias modul.name)
            |> Dict.fromList
    , typs =
        modul.typs
            |> List.map (convertTypDef modul.name)
            |> Dict.fromList
    , values =
        modul.values
            |> List.map (convertValue modul.name)
            |> Dict.fromList
    }


convertAlias : String -> DocJson.Alias -> ( TypId, TypAlias a )
convertAlias modul a =
    ( ( modul, a.name )
    , { typId = ( modul, a.name )
      , args = a.args
      , typ = a.typ |> parseTyp |> typParseErr
      }
    )


convertCase : ( String, List String ) -> ( String, List (FTyp a) )
convertCase ( name, args ) =
    ( name
    , args
        |> List.map (parseTyp >> typParseErr)
    )


convertTypDef : String -> DocJson.Typ -> ( TypId, TypDef a )
convertTypDef modul typ =
    ( ( modul, typ.name )
    , { typId = ( modul, typ.name )
      , args = typ.args
      , constructors =
            typ.cases
                |> List.map convertCase
                |> Dict.fromList
      }
    )


convertValue : String -> DocJson.Value -> ( ValueId, ( FTyp a, Value ) )
convertValue modul val =
    ( modul ++ "." ++ val.name, ( parseTyp val.typ |> typParseErr, () ) )


docJsonToTypEnv : List DocJson.Modul -> TypEnv
docJsonToTypEnv moduls =
    moduls |> List.map modulToTypEnv |> List.foldr Dict.union Dict.empty


modulToTypEnv : DocJson.Modul -> TypEnv
modulToTypEnv modul =
    Dict.union
        initTypEnv
        (Dict.union
            (modul.aliases
                |> List.map (\a -> ( ( modul.name, a.name ), ( a.args, False ) ))
                |> Dict.fromList
            )
            (modul.typs
                |> List.map (\t -> ( ( modul.name, t.name ), ( t.args, True ) ))
                |> Dict.fromList
            )
        )
