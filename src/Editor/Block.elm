module Editor.Block exposing (grid, list)

import Html exposing (..)


list : Direction -> List a -> Html msg
list dir list =
    span []
        [ text (toString dir)
        , span [] (list |> List.map (\i -> text "."))
        ]


grid : Direction -> List (List a) -> Html msg
grid dir list =
    span [] [ text <| toString dir, text <| toString list ]
