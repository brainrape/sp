module Editor.Editor exposing (updateKeyChange)

import App exposing (..)
import Basics.Extra exposing (flip)
import Editor.Keyboard exposing (..)
import Editor.PureTree exposing (..)
import Fi exposing (fi)
import Keyboard exposing (Key(..), KeyChange(..))
import Structure.Direction exposing (..)
import Structure.Edit exposing (..)
import Structure.PureTree exposing (..)
import Structure.Syntax exposing (..)
import Structure.Tree exposing (..)


updateKeyChange : List Key -> Maybe KeyChange -> Model -> Model
updateKeyChange pressedKeys change model =
    let
        ctx =
            model.ctx

        pureTreeCtx =
            model.pureTreeCtx

        upSyn syn model_ =
            { model_ | syn = syn, tree = toTreeSyntax syn }

        move dir =
            { model
                | ctx =
                    { ctx
                        | cursor =
                            ctx.cursor
                                |> Structure.Tree.moveF dir model.tree
                                |> (\c ->
                                        fi (c == []) ctx.cursor c
                                   )

                        --|> Maybe.andThen (Structure.TypAlias.moveFTypAlias dir model.typAlias)
                        --|> Maybe.Extra.orElse ctx.cursor
                    }
            }

        puretree msg_ =
            let
                ( ctx_, tree_ ) =
                    Editor.PureTree.update msg_ ( model.pureTreeCtx, model.pureTree )
            in
            { model | pureTreeCtx = ctx_, pureTree = tree_ }
    in
    keyBindings ( model.pureTreeCtx, model.pureTree )
        |> List.map Tuple.second
        |> matchBindings pressedKeys
        |> Maybe.map puretree
        |> Maybe.withDefault model



-- KeyDown ArrowLeft ->
--     move Left
--
-- KeyDown ArrowRight ->
--     move Right
--
-- KeyDown ArrowUp ->
--     move Up
--
-- KeyDown ArrowDown ->
--     move Down
-- KeyUp (Character ",") ->
--     let
--         ( cursor, syn ) =
--             insertTuple model.ctx.cursor model.syn
--     in
--     upSyn syn { model | syn = syn, ctx = { ctx | cursor = cursor } }
--dir |> Maybe.map (flip move cursor) |> Maybe.withDefault cursor } }
-- let
--     ( cursor, syn, dir ) =
--         doBackspace model.ctx.cursor model.syn
-- in
-- upSyn syn { model | ctx = { ctx | cursor = dir |> Maybe.map (move >> .ctx >> .cursor) |> Maybe.withDefault cursor } }
