module Editor.Keyboard exposing (KeyBinding(..), matchBinding, matchBindings, toString)

import Keyboard exposing (..)
import List exposing (..)
import List.Extra exposing (..)
import Maybe exposing (andThen, withDefault)
import Structure.Direction exposing (..)
import Structure.Typ exposing (..)
import Structure.TypAlias exposing (..)


type KeyBinding msg
    = BindArrows (List Key) (Dir8 -> msg)
    | BindKeys (List Key) msg


matchBindings : List Key -> List (KeyBinding msg) -> Maybe msg
matchBindings pressedKeys bindings =
    bindings
        |> map (matchBinding pressedKeys)
        |> filter ((/=) Nothing)
        |> head
        |> andThen identity


matchBinding : List Key -> KeyBinding msg -> Maybe msg
matchBinding pressedKeys binding =
    case binding of
        BindArrows mods msgf ->
            if mods |> all (\x -> member x pressedKeys) then
                if length pressedKeys == length mods + 1 then
                    getDir4 pressedKeys |> Maybe.map (dir4to8 >> msgf)

                else if length pressedKeys == length mods + 2 then
                    getDir8 pressedKeys |> Maybe.map msgf

                else
                    Nothing

            else
                Nothing

        BindKeys keys msg ->
            Debug.log "bbb" ( pressedKeys, keys, msg )
                |> always
                    (if
                        (keys |> all (\x -> member x pressedKeys))
                            && (length pressedKeys == length keys)
                     then
                        Just msg

                     else
                        Nothing
                    )


getDir4 : List Key -> Maybe Dir4
getDir4 keys =
    keys
        |> map arrowDir
        |> filter ((/=) Nothing)
        |> head
        |> andThen identity


getDir8 : List Key -> Maybe Dir8
getDir8 keys =
    keys
        |> map arrowDir
        |> filter ((/=) Nothing)
        |> take 2
        |> (\x ->
                case x of
                    [ Just arr1, Just arr2 ] ->
                        dir44to8 arr1 arr2

                    _ ->
                        Nothing
           )


arrowDir : Key -> Maybe Dir4
arrowDir key =
    case key of
        ArrowUp ->
            Just Up

        ArrowDown ->
            Just Down

        ArrowLeft ->
            Just Left

        ArrowRight ->
            Just Right

        _ ->
            Nothing


toString : Key -> String
toString key =
    case key of
        Character " " ->
            "⎵"

        Character c ->
            String.toUpper c

        ArrowUp ->
            "⇧"

        ArrowDown ->
            "⇩"

        ArrowLeft ->
            "⇦"

        ArrowRight ->
            "⇨"

        Control ->
            "Ctrl"

        Alt ->
            "Alt"

        Meta ->
            "Meta"

        Shift ->
            "Shift"

        Enter ->
            "↵Enter"

        Tab ->
            "↹Tab"

        Escape ->
            "Esc"

        Backspace ->
            "⌫"

        Delete ->
            "Del"

        _ ->
            Debug.toString key
