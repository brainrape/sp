module Editor.Literal exposing (..)

import Html exposing (Html)

type alias LiteralEditor msg model viewModel =
    Editor () msg () model viewModel
