module Editor.PureTree exposing (Ctx, Edit(..), Msg(..), initCtx, keyBindings, move, stepCtx, update)

import Editor.Keyboard exposing (..)
import Fi exposing (fi)
import Keyboard exposing (..)
import List.Extra exposing (getAt, last)
import Structure.Direction exposing (..)
import Structure.PureTree exposing (..)


type alias Ctx =
    { here : Focus
    , cursor : Maybe Focus
    , pointer : Maybe Focus
    }


initCtx : Ctx
initCtx =
    { here = FHere
    , cursor = Just FHere
    , pointer = Nothing
    }


stepCtx : Int -> Ctx -> Ctx
stepCtx n ctx =
    { ctx
        | here = addF n ctx.here
        , cursor = ctx.cursor |> Maybe.andThen (stepF n)
        , pointer = ctx.pointer |> Maybe.andThen (stepF n)
    }


type Edit
    = Split Dir4
    | Add
    | Remove


type Msg
    = Cursor (Maybe Focus)
    | Pointer (Maybe Focus)
    | Edit Edit
    | Move Dir8


type alias Model =
    ( Ctx, Tree )


keyBindings : Model -> List ( String, KeyBinding Msg )
keyBindings ( ctx, tree ) =
    [ ( "Move", BindArrows [] Move )
    , ( "Split", BindArrows [ Control ] (Edit << Split << dir8to4f) )
    , ( "Extend", BindKeys [ Character " " ] (Edit Add) )
    , ( "Remove", BindKeys [ Backspace ] (Edit Remove) )
    ]


splitM : Dir4 -> Focus -> Tree -> Maybe ( Focus, Tree )
splitM dir cursor tree =
    split dir cursor tree
        |> Maybe.andThen
            (\t ->
                case eqOriDir8 (dir4toOri dir) (dir4to8 dir) of
                    Just True ->
                        Just ( cursor |> addF 1, t )

                    Just False ->
                        Just ( cursor |> addF 0, t )

                    Nothing ->
                        Nothing
            )


addM : Focus -> Tree -> Maybe ( Focus, Tree )
addM cursor tree =
    add cursor tree
        |> Maybe.map (\t -> ( moveF (getLastN cursor + 1) cursor, t ))


removeM : Focus -> Tree -> Maybe ( Focus, Tree )
removeM cursor tree =
    remove cursor tree
        |> Maybe.map
            (\tree_ ->
                if getLastN cursor == 0 then
                    ( drillF Left cursor tree_, tree_ )

                else
                    ( drillF Right (moveF (getLastN cursor - 1) cursor) tree_, tree_ )
            )


edit : Edit -> Focus -> Tree -> Maybe ( Focus, Tree )
edit e cursor tree =
    case Debug.log "e" e of
        Split dir ->
            splitM dir cursor tree

        Add ->
            addM cursor tree

        Remove ->
            removeM cursor tree


move : Dir8 -> Focus -> Tree -> Maybe Focus
move dir foc tree =
    case ( foc, tree ) of
        ( FNode n foc_, Node ori trees ) ->
            case trees |> getAt n |> Maybe.andThen (move dir foc_) of
                Just foc__ ->
                    Just (FNode n foc__)

                Nothing ->
                    let
                        drilled m =
                            trees |> getAt m |> Maybe.map (FNode m << drill (reverse (dir8to4f dir)))

                        go forward =
                            if forward && n < List.length trees then
                                drilled (n + 1)

                            else if n > 0 then
                                drilled (n - 1)

                            else
                                Nothing
                    in
                    eqOriDir8 ori dir |> Maybe.andThen go

        _ ->
            Nothing


update : Msg -> Model -> Model
update msg ( ctx, tree ) =
    case ( msg, ctx.cursor ) of
        ( Edit e, Just cursor ) ->
            edit e cursor tree
                |> Maybe.map (\( c, t ) -> ( { ctx | cursor = Just c }, t ))
                |> Maybe.withDefault ( ctx, tree )

        ( Edit _, Nothing ) ->
            ( ctx, tree )

        ( Move dir, Just cursor ) ->
            move dir cursor tree
                |> Maybe.map (\c -> ( { ctx | cursor = Just c, pointer = Nothing }, tree ))
                |> Maybe.withDefault ( ctx, tree )

        ( Move _, Nothing ) ->
            ( ctx, tree )

        ( Cursor foc, _ ) ->
            ( { ctx | cursor = foc, pointer = Nothing }, tree )

        ( Pointer foc, _ ) ->
            ( { ctx | pointer = foc }, tree )
