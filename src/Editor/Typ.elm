module Editor.Typ exposing (editTyp)

import Html exposing (..)
import Structure.Typ exposing (..)




editTyp : Typ -> Html msg
editTyp typ =
    text <| toStringTyp typ
