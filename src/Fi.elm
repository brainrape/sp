module Fi exposing (fi)


fi : Bool -> a -> a -> a
fi p x y =
    if p then
        x

    else
        y
