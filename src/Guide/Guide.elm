module Guide.Guide exposing (..)

import Editor.PureTree exposing (..)

type alias Model =
    { step : Int
    }


init : Model
init =
    { step = 0
    }


type alias Step =
    { title : String
    , task : String
    , initTree : Tree
    , check : Ctx -> Tree -> Bool
    }



viewStep : Step -> Ctx -> Tree -> Html msg
viewStep step ctx tree =
    div []
        [ h4 [] [text title]
        ,p [] [text task]
        ]


step0 : Step
step0 =
    { title = "This is a Hole."
    , task = "Click or tap to select it."
    , initTree = Hole
    , check ctx tree = ctx.cursor == Just FHere
    }


step1 : Step
step1 =
    { title = "Selected Hole can be split."
    , task = "Click or press the | or - buttons under Actions to split the Hole horizontally or vertically"
    , initTree = Hole
    , check ctx tree = False
    }


steps : List Step
steps = [step0, step1]
