port module Main exposing (blurs, getCoreDocs, main, update)

import App exposing (..)
import Browser
import Browser.Events exposing (onAnimationFrame)
import Ctx exposing (..)
import Dict
import Editor.Editor exposing (updateKeyChange)
import Editor.Keyboard exposing (..)
import Editor.PureTree
import Editor.Typ
import Elm.Docs
import Fi exposing (fi)
import Http
import Json.Decode
import Keyboard exposing (Key(..), KeyChange(..))
import Keyboard.Arrows
import List.Extra exposing (unconsLast)
import Maybe.Extra exposing (isJust)
import Structure.Direction exposing (..)
import Structure.Env
import Structure.Syntax exposing (..)
import Structure.Tree exposing (..)
import Structure.Typ exposing (..)
import Structure.TypAlias exposing (..)
import Structure.TypEnv
import Time
import View.App


port blurs : (() -> msg) -> Sub msg


main : Program () Model Msg
main =
    Browser.document
        { init = \() -> ( initModel, Cmd.none ) --getCoreDocs )
        , update = update
        , view = \model -> { title = "sp", body = View.App.viewApp model }
        , subscriptions =
            \_ ->
                Sub.batch
                    [ Sub.map KeyMsg Keyboard.subscriptions
                    , blurs (\_ -> Blur)
                    , onAnimationFrame Tick
                    ]
        }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        pure model_ =
            ( model_, Cmd.none )

        ctx =
            model.ctx
    in
    case msg of
        GotDocs result ->
            case result of
                Ok docs ->
                    pure { model | docs = Debug.log "docs" docs }

                Err err ->
                    pure model

        Pointer loc ->
            pure { model | ctx = { ctx | pointer = loc } }

        Cursor loc ->
            pure { model | ctx = { ctx | cursor = loc, pointer = [] } }

        KeyMsg keyMsg ->
            let
                ( pressedKeys, change ) =
                    Keyboard.updateWithKeyChange Keyboard.anyKey keyMsg model.pressedKeys
            in
            case change of
                Just keyChange ->
                    pure <|
                        updateKeyChange pressedKeys
                            change
                            { model | pressedKeys = pressedKeys, keyChanges = keyChange :: model.keyChanges }

                Nothing ->
                    pure <|
                        updateKeyChange pressedKeys
                            Nothing
                            { model | pressedKeys = pressedKeys }

        PureTreeMsg m ->
            let
                ( c, t ) =
                    Editor.PureTree.update m ( model.pureTreeCtx, model.pureTree )
            in
            pure { model | pureTreeCtx = c, pureTree = t }

        Blur ->
            pure { model | pressedKeys = [] }

        Tick time ->
            pure { model | time = Time.posixToMillis time }


getCoreDocs : Cmd Msg
getCoreDocs =
    Http.send GotDocs <|
        Http.get
            "../elm-lang--core--5.1.1--documentation.json"
            (Json.Decode.list Elm.Docs.decoder)
