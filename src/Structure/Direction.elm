module Structure.Direction exposing (Dir4(..), Dir8(..), Orientation(..), along, dir44to8, dir4to8, dir4toOri, dir8to4, dir8to4f, eq48, eqOriDir8, flexDir, moveIntRight, moveList, reverse, rotate, rotateLeft, rotateRight, unLR)

import List.Extra exposing (..)


type Orientation
    = Horizontal
    | Vertical


type Dir4
    = Right
    | Up
    | Left
    | Down


type Dir8
    = DirE
    | DirNE
    | DirN
    | DirNW
    | DirW
    | DirSW
    | DirS
    | DirSE


dir4toOri : Dir4 -> Orientation
dir4toOri dir4 =
    case dir4 of
        Right ->
            Horizontal

        Up ->
            Vertical

        Left ->
            Horizontal

        Down ->
            Vertical


eqOriDir8 : Orientation -> Dir8 -> Maybe Bool
eqOriDir8 ori dir8 =
    case ( ori, dir8 ) of
        ( Vertical, DirN ) ->
            Just False

        ( Vertical, DirNE ) ->
            Just False

        ( Vertical, DirNW ) ->
            Just False

        ( Vertical, DirS ) ->
            Just True

        ( Vertical, DirSE ) ->
            Just True

        ( Vertical, DirSW ) ->
            Just True

        ( Vertical, _ ) ->
            Nothing

        ( Horizontal, DirW ) ->
            Just False

        ( Horizontal, DirNW ) ->
            Just False

        ( Horizontal, DirSW ) ->
            Just False

        ( Horizontal, DirE ) ->
            Just True

        ( Horizontal, DirNE ) ->
            Just True

        ( Horizontal, DirSE ) ->
            Just True

        ( Horizontal, _ ) ->
            Nothing


eq48 : Dir4 -> Dir8 -> Bool
eq48 dir4 dir8 =
    case ( dir4, dir8 ) of
        ( Right, DirE ) ->
            True

        ( Right, DirSE ) ->
            True

        ( Right, DirNE ) ->
            True

        ( Up, DirN ) ->
            True

        ( Up, DirNE ) ->
            True

        ( Up, DirNW ) ->
            True

        ( Left, DirW ) ->
            True

        ( Left, DirNW ) ->
            True

        ( Left, DirSW ) ->
            True

        ( Down, DirS ) ->
            True

        ( Down, DirSE ) ->
            True

        ( Down, DirSW ) ->
            True

        _ ->
            False


dir8to4 : Dir8 -> Maybe Dir4
dir8to4 dir8 =
    case dir8 of
        DirE ->
            Just Right

        DirN ->
            Just Up

        DirW ->
            Just Left

        DirS ->
            Just Down

        _ ->
            Nothing


dir8to4f : Dir8 -> Dir4
dir8to4f dir8 =
    case dir8 of
        DirE ->
            Right

        DirNE ->
            Right

        DirN ->
            Up

        DirNW ->
            Up

        DirW ->
            Left

        DirSW ->
            Left

        DirS ->
            Down

        DirSE ->
            Down


dir4to8 : Dir4 -> Dir8
dir4to8 dir4 =
    case dir4 of
        Right ->
            DirE

        Up ->
            DirN

        Left ->
            DirW

        Down ->
            DirS


dir44to8 : Dir4 -> Dir4 -> Maybe Dir8
dir44to8 d1 d2 =
    case ( d1, d2 ) of
        ( Right, Up ) ->
            Just DirNE

        ( Right, Down ) ->
            Just DirSE

        ( Up, Left ) ->
            Just DirNW

        ( Up, Right ) ->
            Just DirNE

        ( Left, Up ) ->
            Just DirNW

        ( Left, Down ) ->
            Just DirSW

        ( Down, Left ) ->
            Just DirSW

        ( Down, Right ) ->
            Just DirSE

        _ ->
            Nothing


unLR : Dir4 -> a -> a -> Maybe a
unLR dir x y =
    case dir of
        Left ->
            Just x

        Right ->
            Just y

        _ ->
            Nothing


rotateRight : Dir4 -> Dir4
rotateRight dir =
    case dir of
        Up ->
            Right

        Right ->
            Down

        Down ->
            Left

        Left ->
            Up


rotateLeft : Dir4 -> Dir4
rotateLeft dir =
    case dir of
        Up ->
            Left

        Right ->
            Up

        Down ->
            Right

        Left ->
            Down


rotate : Dir4 -> Dir4 -> Dir4
rotate to from =
    case to of
        Right ->
            from

        Down ->
            rotateRight to

        Left ->
            reverse to

        Up ->
            rotateLeft to


reverse : Dir4 -> Dir4
reverse dir =
    case dir of
        Up ->
            Down

        Down ->
            Up

        Left ->
            Right

        Right ->
            Left


along : Dir4 -> Dir4 -> Dir4
along dir ref =
    if dir == ref then
        Right

    else if dir == reverse ref then
        Left

    else if dir == rotateLeft ref then
        Up

    else
        Down


moveIntRight : Dir4 -> Int -> Int
moveIntRight dir i =
    case dir of
        Right ->
            i + 1

        Left ->
            i - 1

        _ ->
            i


moveList : Dir4 -> Int -> List a -> Maybe ( Int, a )
moveList dir n list =
    case dir of
        Left ->
            if n > 0 then
                list |> getAt (n - 1) |> Maybe.map (\x -> ( n - 1, x ))

            else
                Nothing

        Right ->
            if n < List.length list then
                list |> getAt (n + 1) |> Maybe.map (\x -> ( n + 1, x ))

            else
                Nothing

        _ ->
            Nothing


flexDir : Dir4 -> String
flexDir dir =
    case dir of
        Up ->
            "column-reverse"

        Down ->
            "column"

        Left ->
            "row-reverse"

        Right ->
            "row"
