module Structure.Edit exposing (Change, Edit, doBackspace, goBackspace, insertTuple, makeTuple, makeTupleLoc)

import Basics.Extra exposing (flip)
import Fi exposing (fi)
import List.Extra exposing (..)
import Maybe.Extra
import Structure.Direction exposing (..)
import Structure.Syntax exposing (..)
import Structure.Tree exposing (..)
import Structure.Typ exposing (..)
import Structure.TypAlias exposing (..)


type alias Edit =
    ()


type alias Change =
    ( Locus, Syntax, Maybe Dir4 )


makeTuple : Locus -> Syntax -> Maybe Syntax
makeTuple loc syn =
    case ( getLastF loc, get (chopF loc) syn, get loc syn ) of
        ( Just (FSynTyp (FTypTuple n)), Just (SynTyp (TypTuple typs)), Just (SynTyp _) ) ->
            set (chopF loc) syn (SynTyp (TypTuple (List.take (n + 1) typs ++ [ TypHole ] ++ List.drop (n + 1) typs)))

        ( _, _, Just (SynTyp typ) ) ->
            set loc syn (SynTyp (TypTuple [ typ, TypHole ]))

        _ ->
            Nothing


makeTupleLoc : Locus -> Locus
makeTupleLoc loc =
    case unconsLast loc of
        Just ( FHere, loc_ ) ->
            makeTupleLoc loc_ ++ [ FHere ]

        Just ( FSynTyp (FTypTuple n), loc_ ) ->
            loc_ ++ [ FSynTyp (FTypTuple (n + 1)) ]

        Just ( foc, loc_ ) ->
            loc_ ++ [ foc, FSynTyp (FTypTuple 1) ]

        _ ->
            loc


insertTuple : Locus -> Syntax -> ( Locus, Syntax )
insertTuple loc syn =
    makeTuple loc syn
        |> Maybe.map (\syn_ -> ( makeTupleLoc loc, syn_ ))
        |> Maybe.withDefault ( loc, syn )


goBackspace : Locus -> Syntax -> Maybe Change
goBackspace loc syn =
    let
        f n typs con conF =
            if List.length typs > 2 then
                set (chopF loc) syn (SynTyp (con (List.take n typs ++ List.drop (n + 1) typs)))
                    |> Maybe.map
                        (\syn_ ->
                            ( fi (n == 0)
                                loc
                                (loc |> setNextToLast (FSynTyp (conF (n - 1))))
                            , syn_
                            , Nothing
                            )
                        )

            else
                fi (List.length typs < 2) 0 (fi (n == 0) 1 0)
                    |> (\i -> typs |> getAt i)
                    |> Maybe.map SynTyp
                    |> Maybe.andThen (set (chopF loc) syn)
                    |> Maybe.map (\syn_ -> ( loc |> removeNextToLast, syn_, Nothing ))
    in
    case ( List.reverse loc, get (chopF loc) syn, get loc syn ) of
        ( FHere :: (FSynTyp (FTypTuple n)) :: _, Just (SynTyp (TypTuple typs)), Just (SynTyp TypHole) ) ->
            f n typs TypTuple FTypTuple

        ( FHere :: (FSynTyp (FTypTuple n)) :: _, Just (SynTyp (TypTuple typs)), Just (SynTyp _) ) ->
            set loc syn (SynTyp TypHole)
                |> Maybe.map (\syn_ -> ( loc, syn_, Nothing ))

        ( FHere :: (FSynTyp (FTypFun n)) :: _, Just (SynTyp (TypFun typs)), Just (SynTyp TypHole) ) ->
            f n typs TypFun FTypFun

        ( FHere :: (FSynTyp (FTypFun n)) :: _, Just (SynTyp (TypFun typs)), Just (SynTyp _) ) ->
            set loc syn (SynTyp TypHole)
                |> Maybe.map (\syn_ -> ( loc, syn_, Nothing ))

        ( FHere :: (FSynTyp (FTypConArg n)) :: _, Just (SynTyp (TypCon _ typs)), Just (SynTyp TypHole) ) ->
            Just ( loc, syn, Just Left )

        ( FHere :: (FSynTyp (FTypConArg n)) :: _, Just (SynTyp (TypCon _ typs)), Just (SynTyp _) ) ->
            set loc syn (SynTyp TypHole)
                |> Maybe.map (\syn_ -> ( loc, syn_, Nothing ))

        ( FHere :: (FSynTyp FTypConTypId) :: _, _, Just (SynTyp (TypCon con typs)) ) ->
            if con == ( "", "" ) then
                Just ( loc |> removeNextToLast, syn, Nothing )

            else
                set loc syn (SynTyp (TypCon ( "", "" ) typs))
                    |> Maybe.map (\syn_ -> ( loc, syn_, Nothing ))

        ( FHere :: (FSynTyp (FTypRecordTyp n)) :: _, Just (SynTyp (TypRecord fields)), Just (SynTyp TypHole) ) ->
            Just ( loc |> setNextToLast (FSynTyp (FTypRecordName n)), syn, Nothing )

        ( FHere :: (FSynTyp (FTypRecordName n)) :: _, _, Just (SynTyp (TypRecord fields)) ) ->
            fields
                |> getAt n
                |> Maybe.andThen
                    (\( name, typ ) ->
                        if name /= "" then
                            set loc syn (SynTyp (TypRecord (fields |> setAt n ( "", typ ))))
                                |> Maybe.map (\syn_ -> ( loc, syn_, Nothing ))

                        else if typ /= TypHole then
                            Just ( loc |> setNextToLast (FSynTyp (FTypRecordTyp n)), syn, Nothing )

                        else if List.length fields > 1 then
                            set (chopF loc) syn (SynTyp (TypRecord (fields |> removeAt n)))
                                |> Maybe.map
                                    (\syn_ ->
                                        (\loc_ -> ( loc_, syn_, Nothing ))
                                            (fi (n < (List.length fields - 1))
                                                loc
                                                (loc |> setNextToLast (FSynTyp (FTypRecordName (n - 1))))
                                            )
                                    )

                        else
                            set (chopF loc) syn (SynTyp TypHole)
                                |> Maybe.map (\syn_ -> ( loc |> removeNextToLast, syn_, Nothing ))
                    )

        ( FHere :: _, _, Just (SynTyp _) ) ->
            set loc syn (SynTyp TypHole)
                |> Maybe.map (\syn_ -> ( loc, syn_, Nothing ))

        _ ->
            Nothing


doBackspace : Locus -> Syntax -> Change
doBackspace loc syn =
    goBackspace loc syn |> Maybe.withDefault ( loc, syn, Nothing )
