module Structure.Docs exposing (Modul, Typ, jsonDecModul, jsonDecTyp)

import Json.Decode as D


type alias Typ =
    { name : String
    , signature : String
    }


type alias Modul =
    { moduleName : String
    , typs : List Typ
    }


jsonDecTyp : D.Decoder Typ
jsonDecTyp =
    D.map2 Typ
        (D.field "name" D.string)
        (D.field "signature" D.string)


jsonDecModul : D.Decoder Modul
jsonDecModul =
    D.map2 Modul
        (D.field "moduleName" D.string)
        (D.field "types" (D.list jsonDecTyp))
