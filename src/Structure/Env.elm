module Structure.Env exposing (Env, ModulEnv, ModulId, Package, PackageId, emptyEnv, overrideEnv)

import Dict exposing (..)
import Structure.Typ exposing (..)
import Structure.TypAlias exposing (..)
import Structure.TypDef exposing (..)
import Structure.Value exposing (..)


type alias ModulId =
    String


type alias PackageId =
    String


type alias Package =
    { name : String }


type alias Env =
    { typs : Dict TypId TypDef
    , aliases : Dict TypId TypAlias
    , values : Dict ValueId ( Typ, Value )
    }


type alias ModulEnv =
    List ModulId


emptyEnv : Env
emptyEnv =
    { typs = empty
    , aliases = empty
    , values = empty
    }


overrideEnv : Env -> Env -> Env
overrideEnv new env =
    { typs = union new.typs env.typs
    , aliases = union new.aliases env.aliases
    , values = union new.values env.values
    }
