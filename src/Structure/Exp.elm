module Structure.Exp exposing (Exp(..), FExp(..), VarName)


type alias VarName =
    String


type Exp
    = ExpApply (List Exp)
    | ExpFun (List VarName) Exp
    | ExpCase (List ( Exp, Exp ))
    | ExpList (List Exp)
    | ExpTuple (List Exp)
    | ExpRecord (List ( VarName, Exp ))
    | ExpVar VarName
    | ExpHole


type FExp
    = FExpApply Int
    | FExpFunArg Int
    | FExpFunExp
    | FExpCasePattern Int
    | FExpCaseExp Int
    | FExpList Int
    | FExpTuple Int
    | FExpRecordName Int
    | FExpRecordVal Int
