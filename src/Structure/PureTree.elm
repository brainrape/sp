module Structure.PureTree exposing (Focus(..), Tree(..), add, addF, drill, drillF, focusToString, get, getLastF, getLastN, gridable, init, isDir, moveF, oneUp, remove, removeF, set, split, stepF)

import List.Extra exposing (getAt, last, setAt)
import Structure.Direction exposing (..)


type Tree
    = Hole
    | Node Orientation (List Tree)


type Focus
    = FHere
    | FNode Int Focus


focusToString : Focus -> String
focusToString foc =
    case foc of
        FHere ->
            "⬤"

        FNode n foc_ ->
            String.fromInt n ++ " " ++ focusToString foc_



-- TODO: multiselect
-- TODO: invariant: all splits have at least one element
-- TODO: terms (var/token/literal) (like holes but with values)
-- TODO: syntaxnodes (like nodes but with additional structure)


init : Tree
init =
    Hole


oneUp : Focus -> Focus
oneUp foc =
    case foc of
        FNode n FHere ->
            FHere

        FNode n foc_ ->
            FNode n (oneUp foc_)

        FHere ->
            FHere


get : Focus -> Tree -> Maybe Tree
get foc tree =
    case ( foc, tree ) of
        ( FHere, _ ) ->
            Just tree

        ( FNode n foc_, Node dir trees ) ->
            trees |> getAt n |> Maybe.andThen (get foc_)

        _ ->
            Nothing


set : Focus -> Tree -> Tree -> Maybe Tree
set foc tree new =
    case ( foc, tree ) of
        ( FHere, _ ) ->
            Just new

        ( FNode n foc_, Node dir trees ) ->
            trees
                |> getAt n
                |> Maybe.andThen (\tree_ -> set foc_ tree_ new)
                |> Maybe.map (\tree_ -> Node dir (trees |> setAt n tree_))

        _ ->
            Nothing


moveF : Int -> Focus -> Focus
moveF n foc =
    case foc of
        FNode n_ FHere ->
            FNode n FHere

        FNode n_ foc_ ->
            FNode n_ (moveF n foc_)

        FHere ->
            FHere


addF : Int -> Focus -> Focus
addF n foc =
    case foc of
        FNode n_ foc_ ->
            FNode n_ (addF n foc_)

        FHere ->
            FNode n FHere


removeF : Focus -> Focus
removeF foc =
    case foc of
        FNode n FHere ->
            FHere

        FNode n foc_ ->
            FNode n (removeF foc_)

        FHere ->
            FHere


stepF : Int -> Focus -> Maybe Focus
stepF n foc =
    case foc of
        FNode n_ foc_ ->
            if n_ == n then
                Just foc_

            else
                Nothing

        FHere ->
            Nothing


remove : Focus -> Tree -> Maybe Tree
remove foc tree =
    case get foc tree of
        Just Hole ->
            case ( getLastF foc, get (oneUp foc) tree ) of
                ( FNode n _, Just (Node dir trees) ) ->
                    if List.length trees > 2 then
                        set (oneUp foc) tree (Node dir ((trees |> List.take n) ++ (trees |> List.drop (n + 1))))

                    else if n == 1 then
                        set (oneUp foc) tree (trees |> List.head |> Maybe.withDefault Hole)

                    else
                        set (oneUp foc) tree (trees |> getAt 1 |> Maybe.withDefault (trees |> List.head |> Maybe.withDefault Hole))

                _ ->
                    Nothing

        Just (Node _ _) ->
            set foc tree init

        Nothing ->
            Nothing


split : Dir4 -> Focus -> Tree -> Maybe Tree
split dir foc tree =
    case get foc tree of
        Just x ->
            set foc tree (Node (dir4toOri dir) [ x, Hole ])

        _ ->
            Nothing


add : Focus -> Tree -> Maybe Tree
add foc tree =
    case ( getLastF foc, get (oneUp foc) tree ) of
        ( FNode n _, Just (Node dir trees) ) ->
            set (oneUp foc)
                tree
                (Node dir (List.take (n + 1) trees ++ [ init ] ++ List.drop (n + 1) trees))

        _ ->
            Nothing


isDir : Dir4 -> Tree -> Bool
isDir dir tree =
    case tree of
        Node ori _ ->
            dir4toOri dir == ori

        _ ->
            False


gridable : List Tree -> Bool
gridable trees =
    True
        && (List.length trees > 2)
        && (trees |> List.all (isDir Right))


getLastF : Focus -> Focus
getLastF foc =
    case foc of
        FNode n FHere ->
            FNode n FHere

        FNode _ foc_ ->
            getLastF foc_

        FHere ->
            FHere


getLastN : Focus -> Int
getLastN foc =
    case foc of
        FNode n FHere ->
            n

        FNode _ foc_ ->
            getLastN foc_

        FHere ->
            0


{-| Focus a leaf node
-}
drill : Dir4 -> Tree -> Focus
drill dir tree =
    let
        go ori trees =
            (case ( dir, ori ) of
                ( Left, _ ) ->
                    ( FNode 0, List.head trees )

                ( Right, _ ) ->
                    ( FNode (List.length trees - 1), last trees )

                ( Up, _ ) ->
                    ( FNode 0, List.head trees )

                ( Down, _ ) ->
                    ( FNode (List.length trees - 1), last trees )
            )
                |> (\( f, t ) -> t |> Maybe.map (drill dir >> f))
                |> Maybe.withDefault FHere
    in
    case tree of
        Hole ->
            FHere

        Node ori trees ->
            go ori trees


{-| Recurse down a tree along a focus, and drill from there
-}
drillF : Dir4 -> Focus -> Tree -> Focus
drillF dir foc tree =
    case ( foc, tree ) of
        ( FNode n foc_, Node dir_ trees ) ->
            trees
                |> getAt n
                |> Maybe.map (drillF dir foc_ >> FNode n)
                |> Maybe.withDefault FHere

        ( FNode n foc_, Hole ) ->
            FHere

        ( FHere, _ ) ->
            drill dir tree
