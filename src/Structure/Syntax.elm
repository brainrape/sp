module Structure.Syntax exposing (Focus(..), Locus, Syntax(..), chopF, get, getLastF, rec, recF, recFTyp, recFTypAlias, recTypList, recTypRecord, replaceSyn, set, stepF, toSynFTyp, toSynL, toTyp, toTypAlias, update)

import Dircle exposing (dircle)
import List.Extra exposing (getAt, setAt, unconsLast, updateAt)
import Structure.Direction exposing (..)
import Structure.Typ exposing (..)
import Structure.TypAlias exposing (..)
import Structure.TypDef exposing (..)


type Syntax
    = SynTyp Typ
    | SynTypAlias TypAlias
    | SynTypDef TypDef


type Focus
    = FSynTyp FTyp
    | FSynTypAlias FTypAlias
    | FSynTypDef FTypDef
    | FBranch Int
    | FHere


type alias Locus =
    List Focus


stepF : Focus -> Locus -> Locus
stepF foc loc =
    case loc of
        x :: xs ->
            if x == foc then
                xs

            else
                []

        _ ->
            []


recTypList : (List Typ -> Typ) -> Int -> List Typ -> Maybe ( Syntax, Syntax -> Maybe Syntax )
recTypList con n typs =
    typs
        |> getAt n
        |> Maybe.map
            (\typ ->
                ( SynTyp typ
                , toTyp >> Maybe.map (\typ_ -> SynTyp (con (typs |> setAt n typ_)))
                )
            )


recTypRecord : Int -> List ( TypVarName, Typ ) -> Maybe ( Syntax, Syntax -> Maybe Syntax )
recTypRecord n fields =
    fields
        |> getAt n
        |> Maybe.map
            (\( name, typ ) ->
                ( SynTyp typ
                , toTyp >> Maybe.map (\typ_ -> SynTyp (TypRecord (fields |> setAt n ( name, typ_ ))))
                )
            )


replaceSyn : Syntax -> Syntax -> Maybe Syntax
replaceSyn old new =
    case ( old, new ) of
        ( SynTyp _, SynTyp _ ) ->
            Just new

        ( SynTypAlias _, SynTypAlias _ ) ->
            Just new

        ( SynTypDef _, SynTypDef _ ) ->
            Just new

        _ ->
            Nothing


toTyp : Syntax -> Maybe Typ
toTyp syn =
    case syn of
        SynTyp typ ->
            Just typ

        _ ->
            Nothing


toTypAlias : Syntax -> Maybe TypAlias
toTypAlias syn =
    case syn of
        SynTypAlias typAlias ->
            Just typAlias

        _ ->
            Nothing


recFTyp : FTyp -> Typ -> Maybe ( Syntax, Syntax -> Maybe Syntax )
recFTyp ftyp typ =
    case ( ftyp, typ ) of
        ( FTypConTypId, TypCon typId typs ) ->
            Just ( SynTyp typ, toTyp >> Maybe.map SynTyp )

        ( FTypConArg n, TypCon typId typs ) ->
            recTypList (TypCon typId) n typs

        ( FTypFun n, TypFun typs ) ->
            recTypList TypFun n typs

        ( FTypTuple n, TypTuple typs ) ->
            recTypList TypTuple n typs

        ( FTypRecordName n, TypRecord fields ) ->
            Just ( SynTyp typ, toTyp >> Maybe.map SynTyp )

        ( FTypRecordTyp n, TypRecord fields ) ->
            recTypRecord n fields

        _ ->
            Nothing


recFTypAlias : FTypAlias -> TypAlias -> Maybe ( Syntax, Syntax -> Maybe Syntax )
recFTypAlias ftypAlias typAlias =
    case Debug.log "recFTypAlias" ( ftypAlias, typAlias ) of
        ( FTypAliasTyp, { typ } ) ->
            Just
                ( SynTyp typ
                , toTyp >> Maybe.map (\typ_ -> SynTypAlias { typAlias | typ = typ_ })
                )

        _ ->
            Nothing


recF : Focus -> Syntax -> Maybe ( Syntax, Syntax -> Maybe Syntax )
recF foc syn =
    case Debug.log "recF" ( foc, syn ) of
        ( FSynTyp ftyp, SynTyp typ ) ->
            recFTyp ftyp typ

        ( FHere, SynTyp TypHole ) ->
            Just ( syn, replaceSyn syn )

        ( FSynTypAlias ftypAlias, SynTypAlias typAlias ) ->
            recFTypAlias ftypAlias typAlias

        ( FHere, _ ) ->
            Just ( syn, replaceSyn syn )

        _ ->
            Nothing



-- Return the sub-syntax indicated by the locus, and a function to update it


rec : Locus -> Syntax -> Maybe ( Syntax, Syntax -> Maybe Syntax )
rec loc syn =
    case Debug.log "rec" loc of
        -- FHere :: loc_ ->
        --     rec loc_ syn
        foc :: loc_ ->
            recF foc syn
                |> Maybe.andThen
                    (\( syn_, f_ ) ->
                        rec loc_ syn_
                            |> Maybe.map
                                (\( syn__, f__ ) ->
                                    Debug.log "r" ( syn__, f__ >> Maybe.andThen f_ )
                                )
                    )

        [] ->
            Just ( syn, replaceSyn syn )


get : Locus -> Syntax -> Maybe Syntax
get loc syn =
    rec loc syn
        |> Maybe.map Tuple.first


set : Locus -> Syntax -> Syntax -> Maybe Syntax
set loc syn new =
    rec loc syn
        |> Maybe.andThen (\( syn_, f_ ) -> f_ new)


update : (Syntax -> Maybe Syntax) -> Locus -> Syntax -> Maybe Syntax
update f loc syn =
    rec loc syn
        |> Maybe.andThen (\( s_, f_ ) -> s_ |> f |> Maybe.andThen f_)


chopF : Locus -> Locus
chopF loc =
    case unconsLast loc of
        Just ( FHere, loc_ ) ->
            chopF loc_ ++ [ FHere ]

        Just ( foc, loc_ ) ->
            loc_

        Nothing ->
            loc


getLastF : Locus -> Maybe Focus
getLastF loc =
    case unconsLast loc of
        Just ( FHere, loc_ ) ->
            getLastF loc_

        Just ( foc, loc_ ) ->
            Just foc

        Nothing ->
            Nothing


toSynFTyp : Locus -> Typ -> Syntax
toSynFTyp loc typ =
    case ( loc, typ ) of
        ( (FSynTyp FTypConTypId) :: loc_, TypCon typId args ) ->
            SynTyp (TypCon ( "", "o" ) (args |> List.map (always TypHole)))

        ( (FSynTyp (FTypConArg n)) :: loc_, TypCon typId args ) ->
            SynTyp (TypCon ( "", "" ) (args |> List.map (always TypHole)))

        ( (FSynTyp (FTypRecordName n)) :: loc_, TypRecord fields ) ->
            SynTyp TypHole

        ( (FSynTyp (FTypRecordTyp n)) :: loc_, _ ) ->
            SynTyp TypHole

        ( (FSynTyp (FTypTuple n)) :: loc_, _ ) ->
            SynTyp TypHole

        ( (FSynTyp (FTypFun n)) :: loc_, _ ) ->
            SynTyp TypHole

        _ ->
            SynTyp (TypVar "toSynFTyp error")


toSynL : Locus -> Syntax -> Syntax
toSynL loc syn =
    case ( loc, syn ) of
        ( (FSynTyp ftyp) :: loc_, SynTyp typ ) ->
            toSynFTyp loc typ

        _ ->
            syn



--     case ( loc, syn ) of
--         ( FHere,  ->
--             SynHole
--
--         FSynTyp ftyp ->
--             toSynFTyp ftyp syn
