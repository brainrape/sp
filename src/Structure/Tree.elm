module Structure.Tree exposing (Term(..), Tree(..), drill, isNode, movF, movG, movL, movLL, moveF, moveG, next, nextToLast, notToken, prev, removeNextToLast, renderEmpty, setNextToLast, toTreeSyntax, toTreeTyp, toTreeTypAlias, toTreeTypDef, walkTree)

import Dircle exposing (dircle)
import Fi exposing (fi)
import List.Extra exposing (..)
import Maybe.Extra exposing (isJust)
import Structure.Direction exposing (..)
import Structure.Syntax exposing (..)
import Structure.Typ exposing (..)
import Structure.TypAlias exposing (..)
import Structure.TypDef exposing (..)


type Term
    = TermTypId TypId
    | TermTypVarName TypVarName
    | TermTypField String


type Tree
    = TreeTerm Term
    | TreeToken String
    | TreeNode Dir4 (List ( Focus, Tree ))
    | TreeGrid (List (List ( Focus, Tree )))


isNode : Tree -> Bool
isNode tree =
    case tree of
        TreeNode _ _ ->
            True

        _ ->
            False


notToken : ( Focus, Tree ) -> Bool
notToken ( f, t ) =
    case t of
        TreeToken _ ->
            False

        _ ->
            True


toTreeTypDef : TypDef -> Tree
toTreeTypDef typDef =
    TreeNode Down
        [ ( FBranch 0
          , TreeNode Right
                ([ ( FHere, TreeToken "type " ) ]
                    ++ [ ( FSynTypDef FTypDefTypId, TreeTerm (TermTypId typDef.typId) ) ]
                    ++ (typDef.args |> List.indexedMap (\i x -> ( FSynTypDef (FTypDefArg i), TreeTerm (TermTypVarName x) )))
                    ++ [ ( FHere, TreeToken "=" ) ]
                )
          )
        ]


toTreeTypAlias : TypAlias -> Tree
toTreeTypAlias typAlias =
    TreeNode Down
        [ ( FBranch 0
          , TreeNode Right
                ([ ( FHere, TreeToken "type alias " ) ]
                    ++ [ ( FSynTypAlias FTypAliasTypId, TreeTerm (TermTypId typAlias.typId) ) ]
                    ++ (typAlias.args |> List.indexedMap (\i x -> ( FSynTypAlias (FTypAliasArg i), TreeTerm (TermTypVarName x) )))
                    ++ [ ( FHere, TreeToken "=" ) ]
                )
          )
        , ( FSynTypAlias FTypAliasTyp, toTreeTyp typAlias.typ )
        ]


renderEmpty : TypId -> TypId
renderEmpty i =
    case i of
        ( "", "" ) ->
            ( "", dircle )

        _ ->
            i


toTreeTyp : Typ -> Tree
toTreeTyp typ =
    case typ of
        TypCon typId args ->
            TreeNode Right <|
                [ ( FSynTyp FTypConTypId, TreeTerm (TermTypId (renderEmpty typId)) ) ]
                    ++ fi (List.isEmpty args) [] [ ( FHere, TreeToken "" ) ]
                    ++ (args
                            |> List.indexedMap (\i x -> ( FSynTyp (FTypConArg i), toTreeTyp x ))
                            |> List.intersperse ( FHere, TreeToken "" )
                       )

        TypFun args ->
            TreeNode Right
                (args
                    |> List.indexedMap (\i x -> ( FSynTyp (FTypFun i), toTreeTyp x ))
                    |> List.intersperse ( FHere, TreeToken "→" )
                )

        TypTuple typs ->
            TreeNode Right
                ([ ( FHere, TreeToken "(" ) ]
                    ++ (typs
                            |> List.indexedMap
                                (\i x -> ( FSynTyp (FTypTuple i), toTreeTyp x ))
                            |> List.intersperse ( FHere, TreeToken ", " )
                       )
                    ++ [ ( FHere, TreeToken ")" ) ]
                )

        TypRecord fields ->
            TreeGrid
                ((fields
                    |> List.indexedMap
                        (\i ( n, t ) ->
                            [ ( FHere, TreeToken (fi (i == 0) "{" ", ") )
                            , ( FSynTyp (FTypRecordName i), TreeTerm (TermTypField (fi (n /= "") n dircle)) )
                            , ( FHere, TreeToken ": " )
                            , ( FSynTyp (FTypRecordTyp i), toTreeTyp t )
                            ]
                        )
                 )
                    ++ [ [ ( FHere, TreeToken "}" ) ] ]
                )

        TypVar name ->
            TreeTerm (TermTypVarName name)

        TypHole ->
            TreeTerm (TermTypVarName dircle)


toTreeSyntax : Syntax -> Tree
toTreeSyntax syn =
    case syn of
        SynTyp x ->
            toTreeTyp x

        SynTypAlias x ->
            toTreeTypAlias x

        SynTypDef x ->
            toTreeTypDef x


drill : Dir4 -> Tree -> Locus
drill dir tree =
    let
        go dir_ syns =
            (case dir_ of
                Right ->
                    syns |> Maybe.map List.reverse

                Left ->
                    syns

                Down ->
                    syns

                Up ->
                    syns |> Maybe.map List.reverse
            )
                |> Maybe.map (List.filter notToken)
                |> Maybe.andThen List.head
                |> Maybe.map (\( f, t ) -> f :: drill dir t)
                |> Maybe.withDefault [ FHere ]
    in
    case tree of
        TreeTerm x ->
            [ FHere ]

        TreeToken x ->
            []

        TreeNode dir_ syns ->
            go (along dir dir_) (Just syns)

        TreeGrid grid ->
            go dir (grid |> List.head)


prev : Int -> List ( Focus, Tree ) -> Maybe ( Focus, Tree )
prev i syns =
    syns |> List.take i |> List.reverse |> List.filter notToken |> List.head


next : Int -> List ( Focus, Tree ) -> Maybe ( Focus, Tree )
next i syns =
    syns |> List.drop (i + 1) |> List.filter notToken |> List.head


movL : Dir4 -> Int -> Dir4 -> List ( Focus, Tree ) -> Locus
movL dir i dir_ syns =
    (case along dir dir_ of
        Left ->
            prev i syns

        Right ->
            next i syns

        _ ->
            Nothing
    )
        |> (\r ->
                case r of
                    Just ( FHere, t ) ->
                        drill (reverse dir) t

                    Just ( f, t ) ->
                        [ f ] ++ drill (reverse dir) t

                    Nothing ->
                        []
           )


movLL : Dir4 -> Int -> Int -> List (List ( Focus, Tree )) -> Locus
movLL dir i j grid =
    (case along dir Right of
        Left ->
            grid |> getAt i |> Maybe.andThen (prev j)

        Down ->
            grid |> getAt (i + 1) |> Maybe.andThen (getAt j)

        Right ->
            grid |> getAt i |> Maybe.andThen (\l -> next j l)

        Up ->
            grid |> getAt (i - 1) |> Maybe.andThen (getAt j)
    )
        |> (\r ->
                case r of
                    Just ( FHere, t ) ->
                        drill (reverse dir) t

                    Just ( f, t ) ->
                        [ f ] ++ drill (reverse dir) t

                    Nothing ->
                        []
           )


movF : Dir4 -> Focus -> Dir4 -> List ( Focus, Tree ) -> Locus
movF dir foc dir_ syns =
    syns
        |> findIndex (\( f, _ ) -> f == foc)
        |> Maybe.map (\i -> movL dir i dir_ syns)
        |> Maybe.withDefault []


movG : Dir4 -> Focus -> List (List ( Focus, Tree )) -> Locus
movG dir foc grid =
    grid
        |> List.indexedMap (\i list -> ( i, list, list |> findIndex (\( f, _ ) -> f == foc) ))
        |> List.filter (\( _, _, j ) -> isJust j)
        |> List.head
        |> Maybe.map (\( i, l, j ) -> ( i, l, j |> Maybe.withDefault 0 ))
        |> Maybe.map (\( i, l, j ) -> movLL dir i j grid)
        |> Maybe.withDefault []


moveF : Dir4 -> Tree -> Locus -> Locus
moveF dir tree loc =
    case ( tree, loc ) of
        ( TreeNode dir_ syns, x :: FHere :: [] ) ->
            movF dir x dir_ syns

        ( TreeGrid grid, x :: FHere :: [] ) ->
            movG dir x grid

        ( TreeGrid grid, x :: y :: xs ) ->
            moveG dir grid x (y :: xs)

        ( TreeNode dir_ syns, x :: y :: xs ) ->
            case moveF dir (walkTree x syns) (y :: xs) of
                z :: zs ->
                    x :: z :: zs

                [] ->
                    movF dir x dir_ syns

        _ ->
            loc


moveG : Dir4 -> List (List ( Focus, Tree )) -> Focus -> Locus -> Locus
moveG dir grid x xs =
    grid
        |> List.indexedMap
            (\i list ->
                ( i, list, list |> findIndex (\( f, t ) -> x == f) )
            )
        |> List.filter (\( _, _, j ) -> isJust j)
        |> List.head
        |> Maybe.map (\( i, l, j ) -> ( i, l, j |> Maybe.withDefault 0 ))
        |> Maybe.map
            (\( i, l, j ) ->
                case moveF dir (walkTree x l) xs of
                    z :: zs ->
                        x :: z :: zs

                    [] ->
                        movG dir x grid
            )
        |> Maybe.withDefault []


walkTree : Focus -> List ( Focus, Tree ) -> Tree
walkTree foc syns =
    syns
        |> List.filter (\( f, s ) -> f == foc)
        |> List.map Tuple.second
        |> List.head
        |> Maybe.withDefault (TreeToken "Exception: walkTree: Focus not found")


nextToLast : Locus -> Focus
nextToLast foc =
    case List.reverse foc of
        FHere :: x :: xs ->
            x

        [ x ] ->
            x

        _ ->
            FHere


setNextToLast : Focus -> Locus -> Locus
setNextToLast foc loc =
    case List.reverse loc of
        FHere :: x :: xs ->
            List.reverse (FHere :: foc :: xs)

        _ ->
            loc


removeNextToLast : Locus -> Locus
removeNextToLast loc =
    case List.reverse loc of
        FHere :: x :: xs ->
            List.reverse (FHere :: xs)

        _ ->
            loc
