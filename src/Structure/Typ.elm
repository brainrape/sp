module Structure.Typ exposing (FTyp(..), Typ(..), TypId, TypName, TypParseErr, TypVarName, dragTyp, drillFTyp, ex, exS, exTypCon, exTypFun, exTypRecord, exTypRecord2, exTypTuple, exTypVar, toStringTyp, typEx)

import Fi exposing (fi)
import List.Extra exposing (..)
import Maybe.Extra exposing (..)
import Parser exposing ((|.), (|=))
import Structure.Direction exposing (..)
import Structure.Modul exposing (..)


type alias TypId =
    ( ModulId, String )


type alias TypName =
    String


type alias TypVarName =
    String


type alias TypParseErr =
    ()


type Typ
    = TypVar TypVarName
    | TypCon TypId (List Typ)
    | TypFun (List Typ)
    | TypTuple (List Typ)
    | TypRecord (List ( TypVarName, Typ ))
    | TypHole


type FTyp
    = FTypConTypId
    | FTypConArg Int
    | FTypRecordName Int
    | FTypRecordTyp Int
    | FTypTuple Int
    | FTypFun Int


ex : Typ
ex =
    --fTypCon ( "Result", "Result" ) [ fTypFun (fTypVar "a") (fTypFun (fTypVar "b") (fTypCon ( "List", "List" ) [ fTypVar "a" ])), fTypCon ( "Maybe", "Maybe" ) [ fTypVar "a" ] ]
    TypCon ( "MyResult", "MyResult" )
        [ TypFun
            [ TypVar "a"
            , TypVar "b"
            , TypCon ( "List", "List" )
                [ TypRecord
                    [ ( "foo", TypCon ( "Maybe", "Maybe" ) [ TypVar "a" ] )
                    , ( "barquuxifity", TypTuple [ TypVar "a", TypVar "b" ] )
                    ]
                ]
            ]
        , TypCon ( "Maybe", "Maybe" ) [ TypVar "a" ]
        ]


exS : String
exS =
    toStringTyp ex


toStringTyp : Typ -> String
toStringTyp typ =
    case typ of
        TypVar name ->
            name

        TypCon ( modul, name ) args ->
            "("
                ++ modul
                ++ "."
                ++ name
                ++ fi (List.isEmpty args) "" " "
                ++ String.join ", " (args |> List.map toStringTyp)
                ++ ")"

        TypFun typs ->
            "(" ++ (typs |> List.map toStringTyp |> String.join " -> ") ++ ")"

        TypTuple typs ->
            "( " ++ (typs |> List.map toStringTyp |> List.intersperse ", " |> String.concat) ++ " )"

        TypRecord fields ->
            "{ " ++ (fields |> List.map (\( n, t ) -> n ++ " : " ++ toStringTyp t) |> List.intersperse ", " |> String.concat) ++ "}"

        TypHole ->
            "o"


drillFTyp : Dir4 -> Typ -> List FTyp
drillFTyp dir typ =
    case ( dir, typ ) of
        ( Left, TypCon _ _ ) ->
            [ FTypConTypId ]

        ( Right, TypCon _ args ) ->
            [ FTypConArg (List.length args - 1) ]
                ++ (last args |> Maybe.map (drillFTyp dir) |> Maybe.withDefault [])

        ( Left, TypFun _ ) ->
            [ FTypFun 0 ]

        ( Right, TypFun args ) ->
            [ FTypFun (List.length args - 1) ]
                ++ (last args |> Maybe.map (drillFTyp dir) |> Maybe.withDefault [])

        _ ->
            []


dragTyp : Dir4 -> Typ -> FTyp -> Maybe ( Typ, FTyp )
dragTyp dir typ foc =
    case ( dir, typ ) of
        _ ->
            Just ( typ, foc )


exTypCon : Typ
exTypCon =
    TypCon ( "", "" ) []


exTypFun : Typ
exTypFun =
    TypFun [ TypHole, TypHole ]


exTypVar : Typ
exTypVar =
    TypVar ""


exTypTuple : Typ
exTypTuple =
    TypTuple [ TypHole, TypHole ]


exTypRecord : Typ
exTypRecord =
    TypRecord
        [ ( "", TypHole )
        , ( "", TypHole )
        ]


exTypRecord2 : Typ
exTypRecord2 =
    TypRecord
        [ ( "foo", TypCon ( "", "Int" ) [] )
        , ( "bar", TypCon ( "", "Bool" ) [] )
        ]


typEx : List ( String, List Typ )
typEx =
    [ ( "Variable", [ exTypVar, TypVar "a", TypVar "b" ] )
    , ( "Constructor", [ exTypCon, TypCon ( "", "" ) [ TypHole ], TypCon ( "", "" ) [ TypHole, TypHole ] ] )
    , ( "Function", [ exTypFun, TypFun [ TypHole, TypHole, TypHole ] ] )
    , ( "Tuple", [ exTypTuple, TypTuple [ TypHole, TypHole, TypHole ] ] )
    , ( "Record", [ exTypRecord ] )
    ]
