module Structure.TypAlias exposing (FTypAlias(..), TypAlias, exTypAlias, initTypAlias, renderFTypAlias, toStringTypAlias)

import Fi exposing (fi)
import List.Extra
import Structure.Direction exposing (..)
import Structure.Typ exposing (..)


type alias TypAlias =
    { typId : TypId
    , args : List TypVarName
    , typ : Typ
    }


type FTypAlias
    = FTypAliasTypId
    | FTypAliasArg Int
    | FTypAliasTyp


initTypAlias : TypAlias
initTypAlias =
    { typId = ( "", "" )
    , args = []
    , typ = TypHole
    }


toStringTypAlias : TypAlias -> String
toStringTypAlias { typId, args, typ } =
    "type alias "
        ++ Tuple.first typId
        ++ "."
        ++ Tuple.second typId
        ++ (args |> List.map (\a -> " " ++ a) |> String.join "")
        ++ " = "
        ++ toStringTyp typ


renderFTypAlias : (a -> String) -> TypAlias -> String
renderFTypAlias f typAlias =
    "type alias " ++ Tuple.second typAlias.typId ++ String.concat (List.intersperse " " typAlias.args) ++ " = " ++ toStringTyp typAlias.typ


exTypAlias : TypAlias
exTypAlias =
    { typId = ( "", "" )
    , args = []
    , typ = TypHole
    }
