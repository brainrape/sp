module Structure.TypDef exposing (ConstructorName, FTypDef(..), TypDef, exTypDef, toStringTypDef)

import Fi exposing (fi)
import List.Extra exposing (getAt)
import Maybe.Extra
import Structure.Direction exposing (..)
import Structure.Typ exposing (..)


type alias ConstructorName =
    String


type alias TypDef =
    { typId : TypId
    , args : List TypVarName
    , constructors : List ( ConstructorName, List Typ )
    }


type FTypDef
    = FTypDefTypId
    | FTypDefArg Int
    | FTypDefConName Int
    | FTypDefConTyp Int FTyp


toStringTypDef : TypDef -> String
toStringTypDef typdef =
    "type "
        ++ Tuple.first typdef.typId
        ++ "."
        ++ Tuple.second typdef.typId
        ++ (typdef.args
                |> List.map (\x -> " " ++ x)
                |> String.join ""
           )
        ++ " = "
        ++ (typdef.constructors
                |> List.map
                    (\( ctor, args ) ->
                        ctor
                            ++ (args
                                    |> List.map toStringTyp
                                    |> List.map (\x -> " " ++ x)
                                    |> String.join ""
                               )
                    )
                |> String.join " | "
           )


exTypDef : TypDef
exTypDef =
    { typId = ( "", "◌" )
    , args = []
    , constructors = [ ( "", [] ), ( "", [ TypVar "" ] ) ]
    }
