module Structure.TypEnv exposing (TypEnv, initTypEnv)

import Dict exposing (Dict)
import Structure.Typ exposing (..)


type alias TypEnv =
    Dict TypId ( List String, Bool )


initTypEnv : TypEnv
initTypEnv =
    Dict.fromList
        [ ( ( "", "Bool" ), ( [], True ) )
        , ( ( "", "Float" ), ( [], True ) )
        , ( ( "", "Char" ), ( [], True ) )
        , ( ( "", "String" ), ( [], True ) )
        , ( ( "List", "List" ), ( [ "a" ], True ) )
        ]
