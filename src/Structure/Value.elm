module Structure.Value exposing (Value, ValueId, valueToString)


type alias ValueId =
    String


type alias Value =
    ()


valueToString : Value -> String
valueToString v =
    Debug.toString v
