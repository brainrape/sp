module View.App exposing (css, viewApp)

import App exposing (..)
import Ctx exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Structure.PureTree
import Structure.Tree exposing (..)
import Structure.Typ
import Structure.TypAlias
import View.Env
import View.Keyboard exposing (..)
import View.Palette
import View.PureTree
import View.Tree
import View.TypEnv



-- M
-- τ
-- ƒ
-- ν


viewApp : Model -> List (Html Msg)
viewApp model =
    [ node "link" [ href "https://fonts.googleapis.com/css?family=Fira+Sans:300,300i&amp;subset=latin-ext", rel "stylesheet" ] []
    , node "style" [] [ text css ]
    , div [ class "main", style "height" "90%", style "display" "flex" ]
        [ View.PureTree.view model.pureTreeCtx model.pureTree |> Html.map PureTreeMsg ]

    -- [ div [] [ viewMenu model ]
    --
    -- --, div [] (viewMain model)
    -- , div [] [ View.PureTree.view model.pureTreeCtx model.pureTree |> Html.map PureTreeMsg ]
    -- , viewKeyboard model
    --
    , div []
        [ --viewStatus model
          viewKeyboard model
        ]

    -- --, div [] [ View.Palette.viewPalette model ]
    -- ]
    ]


viewMain : Model -> List (Html Msg)
viewMain model =
    [ View.Tree.viewTree model.ctx (toTreeSyntax model.syn)
    , br [] []
    , br [] []
    , div [ style "display" "none" ]
        [ --, View.DocJson.viewDocJson model.docs
          View.Env.viewEnv model.env
        , View.TypEnv.viewTypEnv model.typenv
        ]
    ]


viewStatus : Model -> Html Msg
viewStatus model =
    table [ style "font-size" "0.8em" ]
        [ tr []
            [ td [] [ div [ style "min-height" "24px" ] [ text "keys" ] ]
            , td []
                [ --text (Debug.toString model.pressedKeys)
                  viewKeyboard model
                ]
            ]
        , tr []
            [ td [] [ span [] [ text "cursor" ] ]

            --, td [] [ span [] [ View.TypAlias.viewFocus initCtx model.ctx.cursor ] ]
            , td [] [ span [] [ text <| Debug.toString model.ctx.cursor ] ]
            ]
        , tr []
            [ td [] [ span [] [ text "pointer" ] ]

            --, td [] [ span [] [ View.TypAlias.viewFocus initCtx model.ctx.pointer ] ]
            , td []
                [ span [] [ text <| Debug.toString model.ctx.pointer ]
                ]
            ]
        ]


viewMenu : Model -> Html.Html Msg
viewMenu model =
    div [] [ text "" ]


css : String
css =
    """
html {
    background : #111;
    color: white;
    font-family: Fira Sans, sans-serif;
    height: 100%;
    overflow: hidden;
}

body {
    height: 100%;
    margin : 0;
}
"""
