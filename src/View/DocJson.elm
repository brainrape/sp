module View.DocJson exposing (viewAlias, viewDocJson, viewModul, viewTyp, viewValue)

import DocJson
import Html exposing (..)


viewAlias : DocJson.Alias -> Html msg
viewAlias alias =
    div [] []


viewValue : DocJson.Value -> Html msg
viewValue value =
    div []
        [ text value.name
        , text value.comment
        , text value.typ
        ]


viewTyp : DocJson.Typ -> Html msg
viewTyp typ =
    div []
        [ text typ.name
        , text typ.comment
        , text <| toString typ.args
        , text <| toString typ.cases
        ]


viewModul : DocJson.Modul -> Html msg
viewModul modul =
    div []
        [ text modul.name
        , text modul.comment
        , div [] (List.map viewAlias modul.aliases)
        , div [] (List.map viewTyp modul.typs)
        , div [] (List.map viewValue modul.values)
        ]


viewDocJson : List DocJson.Modul -> Html msg
viewDocJson moduls =
    div [] (moduls |> List.map viewModul)
