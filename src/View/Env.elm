module View.Env exposing (viewAlias, viewEnv, viewTyp, viewTypDef, viewValue)

import Dict
import Html exposing (..)
import Structure.Env exposing (..)
import Structure.Typ exposing (..)
import Structure.TypAlias exposing (..)
import Structure.TypDef exposing (..)
import Structure.Value exposing (..)


viewEnv : Env -> Html msg
viewEnv env =
    div []
        [ div []
            ([ h4 [] [ text "typs" ] ] ++ (env.typs |> Dict.toList |> List.map viewTypDef))
        , div []
            ([ h4 [] [ text "aliases" ] ] ++ (env.aliases |> Dict.toList |> List.map viewAlias))
        , div []
            ([ h4 [] [ text "values" ] ] ++ (env.values |> Dict.toList |> List.map viewValue))
        ]


viewTyp : ( TypId, Typ ) -> Html msg
viewTyp ( typid, typ ) =
    div []
        [ text <| Debug.toString typid
        , text " = "
        , text <| toStringTyp typ
        ]


viewTypDef : ( TypId, TypDef ) -> Html msg
viewTypDef ( typid, typdef ) =
    div []
        [ text <| toStringTypDef typdef
        ]


viewAlias : ( TypId, TypAlias ) -> Html msg
viewAlias ( typid, alias ) =
    div []
        [ text <| toStringTypAlias alias
        ]


viewValue : ( ValueId, ( Typ, Value ) ) -> Html msg
viewValue ( typid, ( typ, value ) ) =
    div []
        [ text <| Debug.toString typid
        , text " : "
        , text <| toStringTyp typ
        , text " = "
        , text <| valueToString value
        ]
