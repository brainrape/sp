module View.Keyboard exposing (keyboardGlyph, viewArrows, viewBinding, viewBindings, viewKey, viewKeyboard, viewKeys)

import App exposing (..)
import Editor.Keyboard exposing (..)
import Fi exposing (fi)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Keyboard exposing (..)
import Structure.Direction exposing (..)


keyboardGlyph : String
keyboardGlyph =
    "⌨"


viewKeyboard : Model -> Html msg
viewKeyboard model =
    div []
        [ --viewArrows
          text keyboardGlyph
        , text " "
        , viewKeys (List.reverse model.pressedKeys)
        ]


viewKeys : List Key -> Html msg
viewKeys keys =
    span [] (List.map viewKey keys)


viewBindings : List ( String, KeyBinding msg ) -> Html msg
viewBindings bindings =
    div []
        [ div [] [ text "Key Bindings" ]
        , table []
            [ tbody []
                (bindings |> List.map viewBinding)
            ]
        ]


viewBinding : ( String, KeyBinding msg ) -> Html msg
viewBinding ( txt, combo ) =
    tr []
        [ td []
            [ case combo of
                BindArrows mods msgf ->
                    span [] ((mods |> List.map viewKey) ++ [ viewArrows_ msgf ])

                BindKeys keys msg ->
                    span [ onClick msg, style "cursor" "pointer" ] (keys |> List.map viewKey)
            ]
        , td [] [ text txt ]
        ]


viewKey : Key -> Html msg
viewKey key =
    let
        str =
            Editor.Keyboard.toString key
    in
    span
        [ style "border" "2px solid #bbb"
        , style "margin-right" "2px"

        --, style "padding" (fi (String.length str > 1) "1px 1px" "1px 4px")
        , style "padding" "1px 1px"
        , style "border-radius" "3px"
        , style "font-weight" "bold"
        , style "font-size" "1 em"
        , style "box-shadow" "1px 1px #666"
        , style "font-family" "sans-serif"
        , style "min-width" (fi (str == "⎵") "28px" "16px")
        , style "text-align" "center"
        , style "display" "inline-block"
        ]
        [ text <| String.slice 0 1 str
        , span [ style "font-weight" "lighter", style "font-size" "0.6em" ]
            [ text <| String.slice 1 (String.length str) str ]
        ]


viewArrows__ : Maybe (Dir8 -> msg) -> Html msg
viewArrows__ msgf =
    span
        [ style "font-size" "16px"
        , style "flex-direction" "column"
        , style "display" "inline-flex"
        , style "margin-right" "3px"
        , style "line-height" "8px"
        , style "align-items" "flex-start"
        , style "vertical-align" "top"
        ]
    <|
        let
            sty =
                [ style "display" "inline-block"
                , style "border" "2px solid #bbb"
                , style "border-radius" "3px"
                , style "width" "8px"
                , style "height" "8px"
                , style "text-align" "center"
                , style "box-shadow" "1px 1px #666"
                , style "font-size" "0.7em"
                , style "cursor" "pointer"
                ]

            events dir =
                msgf |> Maybe.map (\f -> [ onClick (f dir) ]) |> Maybe.withDefault []
        in
        [ div [ style "display" "flex" ]
            [ span (sty ++ [ style "visibility" "hidden" ]) [ text (toString ArrowLeft) ]
            , span (sty ++ events DirN) [ text (toString ArrowUp) ]
            ]
        , div [ style "display" "flex" ]
            [ span (sty ++ events DirW) [ text (toString ArrowLeft) ]
            , span (sty ++ events DirS) [ text (toString ArrowDown) ]
            , span (sty ++ events DirE) [ text (toString ArrowRight) ]
            ]
        ]


viewArrows_ : (Dir8 -> msg) -> Html msg
viewArrows_ msgf =
    viewArrows__ (Just msgf)


viewArrows : Html msg
viewArrows =
    viewArrows__ Nothing
