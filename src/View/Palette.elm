module View.Palette exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import App exposing (..)
import Structure.Tree exposing (..)
import Structure.Typ exposing (..)
import Structure.TypDef exposing (..)
import Structure.TypAlias exposing (..)
import Structure.Direction exposing (..)
import View.Tree exposing (..)
import Structure.Syntax exposing (..)
import Ctx exposing (..)


item title content =
    div []
        [ div [ style "background" "#222", style "padding-left" "6px" ] [ text title ]
        , div [ style "display" "flex" ] content
        , div [ style "display" "flex", style "height" "6px" ] []
        ]


heading title =
    div
        [ style "border-bottom" "1px solid #444"
        , style "background" "#222"
        , style "font-weight" "bold"
        ]
        [ text title ]


treePaletteDef : Tree
treePaletteDef =
    TreeNode Down
        [ ( FBranch 0, TreeTerm (TermTypId ( "", "Definitions" )) )
        , ( FBranch 1, toTreeTypDef exTypDef )
        , ( FBranch 2, toTreeTypAlias exTypAlias )
        ]


treePaletteTyp : Tree
treePaletteTyp =
    TreeNode Down
        ([ ( FBranch 0, TreeTerm (TermTypId ( "", "Types" )) ) ]
            ++ (typEx
                    |> List.indexedMap
                        (\i ( title, ts ) ->
                            ( FBranch i
                            , TreeNode Down
                                [ ( FBranch 0, TreeTerm (TermTypId ( "", title )) )
                                , ( FBranch 1
                                  , TreeNode Right
                                        (ts
                                            |> List.map toTreeTyp
                                            |> List.indexedMap (\j t -> ( FBranch j, t ))
                                            |> List.intersperse ( FHere, TreeToken " " )
                                        )
                                  )
                                ]
                            )
                        )
               )
        )


viewPalette : Model -> Html Msg
viewPalette model =
    let
        ctx =
            model.ctx |> (\c -> { c | here = [], cursor = [], pointer = [] })
    in
        div
            [ style "display" "inline-flex"
            , style "flex-direction" "column"
            ]
            ([]
                ++ [ vTree ctx treePaletteDef ]
                ++ [ vTree ctx treePaletteTyp ]
             --++ [ heading "Value" ]
             --++ [ text "..." ]
            )
