module View.PureTree exposing (oriStyles, view, viewCtx, viewTree)

import Dircle exposing (dircle)
import Editor.Keyboard exposing (..)
import Editor.PureTree exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode
import Keyboard exposing (Key(..))
import Structure.Direction exposing (..)
import Structure.PureTree exposing (..)
import View.Keyboard exposing (..)


view : Ctx -> Tree -> Html Msg
view ctx tree =
    div [ style "display" "flex", style "width" "100%", style "height" "100%" ]
        [ span [ onMouseLeave (Pointer Nothing), style "display" "inline-flex", style "width" "70%", style "align-items" "flex-start", style "overflow" "auto" ] [ viewTree ctx tree ]
        , span [ style "width" "30%" ] [ viewBindings (keyBindings ( ctx, tree )), br [] [], viewCtx ctx ]
        ]


button : String -> Msg -> Html Msg
button txt msg =
    span
        [ onClick msg
        , style "display" "inline-block"
        , style "padding" "3px"
        , style "cursor" "pointer"
        , style "border" "2px solid #eee"
        , style "border-radius" "3px"
        , style "min-width" "20px"
        , style "margin" "4px"
        , style "text-align" "center"
        , style "background" "#333"
        ]
        [ text txt ]


keyButton : Key -> String -> msg -> Html msg
keyButton key txt msg =
    span [ onClick msg, style "cursor" "pointer", style "margin-top" "12px", style "display" "inline-block" ]
        [ viewKey key
        , text " "
        , text txt
        ]


bindingButton : ( String, KeyBinding msg ) -> msg -> Html msg
bindingButton ( txt, binding ) msg =
    span [ onClick msg, style "cursor" "pointer", style "margin-top" "12px", style "display" "inline-block" ]
        [ viewBinding ( txt, binding )
        , text " "
        , text txt
        ]


viewActions : Ctx -> Tree -> Html Msg
viewActions ctx tree =
    div []
        [ div [] [ text "Key Bindings" ]
        , table []
            [ tbody []
                [ tr [] [ td [] [ viewArrows ], td [] [ text "Move" ] ]
                , tr [] [ td [] [ viewKey Control, viewArrows ], td [] [ text "Split" ] ]
                , tr [] [ td [] [ viewKey Spacebar ], td [] [ text "Extend Right" ] ]
                , tr [] [ td [] [ viewKey Enter ], td [] [ text "Extend Down" ] ]
                , tr [] [ td [] [ viewKey Backspace, text ", ", viewKey Delete ], td [] [ text "Remove" ] ]
                ]
            ]
        ]


viewCtx : Ctx -> Html msg
viewCtx ctx =
    div []
        [ div [] [ text "Focus" ]
        , div [] [ span [] [ text "‸ " ], text (ctx.cursor |> Maybe.map focusToString |> Maybe.withDefault "-") ]
        , div [] [ span [] [ text "🖰 " ], text (ctx.pointer |> Maybe.map focusToString |> Maybe.withDefault "-") ]
        ]


nodeEvents : Ctx -> List (Attribute Msg)
nodeEvents ctx =
    [ stopPropagationOn "mousemove" (Json.Decode.succeed ( Pointer (Just ctx.here), True ))
    , stopPropagationOn "click" (Json.Decode.succeed ( Cursor (Just ctx.here), True ))
    ]


highlight : Ctx -> String
highlight ctx =
    if ctx.pointer == Just FHere then
        "#ddd"

    else if ctx.cursor == Just FHere then
        "#dd8"

    else
        "#444"


nodeHighlight : Ctx -> List (Attribute msg)
nodeHighlight ctx =
    [ style "border-color" (highlight ctx) ]


holeHighlight : Ctx -> List (Attribute msg)
holeHighlight ctx =
    [ style "color" (highlight ctx) ]


viewTree : Ctx -> Tree -> Html Msg
viewTree ctx tree =
    case tree of
        Hole ->
            span
                (nodeEvents ctx ++ holeHighlight ctx ++ holeStyles)
                [ text "⬤" ]

        Node dir trees ->
            span (nodeEvents ctx ++ nodeHighlight ctx ++ nodeStyles dir)
                (trees |> List.indexedMap (\i t -> viewTree (stepCtx i ctx) t))


holeStyles : List (Attribute msg)
holeStyles =
    [ style "padding" "3px"
    , style "cursor" "pointer"
    , style "height" "9px"
    , style "width" "9px"
    , style "line-height" "9px"
    ]


boxStyles : List (Attribute msg)
boxStyles =
    [ style "display" "flex"
    , style "border-width" "1px"
    , style "border-style" "solid"
    , style "border-radius" "6px"
    , style "margin" "1px"
    ]


oriStyles : Orientation -> List (Attribute msg)
oriStyles ori =
    [ style "align-items" "flex-start" ]
        ++ (case ori of
                Horizontal ->
                    [ style "flex-direction" "row" ]

                Vertical ->
                    [ style "flex-direction" "column" ]
           )


nodeStyles : Orientation -> List (Attribute msg)
nodeStyles ori =
    boxStyles ++ oriStyles ori


btnStyles : List (Attribute msg)
btnStyles =
    boxStyles ++ [ style "background" "#8a8" ]
