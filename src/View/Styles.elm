module View.Styles exposing (styl, styles)

import Dict exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)


styles : Dict String (List (Attribute msg))
styles =
    [ ( "", [] )
    ]
        |> List.map (\( class, styls ) -> ( class, styls |> List.map (\( k, v ) -> style k v) ))
        |> Dict.fromList


styl : String -> List (Attribute msg)
styl k =
    styles |> Dict.get k |> Maybe.withDefault []
