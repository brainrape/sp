module View.Theme exposing (Theme)

import Color exposing (..)


type alias Theme =
    { backgroundColor : Color
    , textColor : Color
    , cursorColor : Color
    , pointerColor : Color
    }


blackTheme : Theme
blackTheme =
    { backgroundColor = black
    , textColor = lightGrey
    , cursorColor = lightYellow
    , pointerColor = white
    }


whiteTheme : Theme
whiteTheme =
    { backgroundColor = white
    , textColor = darkGrey
    , cursorColor = darkYellow
    , pointerColor = black
    }
