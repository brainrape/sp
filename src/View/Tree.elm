module View.Tree exposing (vNode, vTerm, vToken, vTree, viewTree)

import App exposing (..)
import Ctx exposing (..)
import Fi exposing (fi)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode
import List.Extra
import Structure.Direction exposing (..)
import Structure.Syntax exposing (..)
import Structure.Tree exposing (..)
import Structure.Typ exposing (..)
import Structure.TypAlias exposing (..)


viewTree : Ctx -> Tree -> Html Msg
viewTree ctx typ =
    div [ style "display" "flex", style "cursor" "pointer" ]
        [ span [ style "display" "flex", onMouseLeave (Pointer []) ]
            [ vTree ctx typ ]
        ]


vTree : Ctx -> Tree -> Html Msg
vTree ctx tree =
    case tree of
        TreeTerm term ->
            vTerm ctx term

        TreeToken str ->
            vToken str

        TreeNode dir syns ->
            vNode ctx dir syns

        TreeGrid grid ->
            vGrid ctx grid


vGrid : Ctx -> List (List ( Focus, Tree )) -> Html Msg
vGrid ctx grid =
    span
        (List.map (\( a, b ) -> style a b)
            [ ( "display", "inline-grid" )
            , ( "grid-template-columns", "repeat(" ++ String.fromInt (grid |> List.head |> Maybe.withDefault [] |> List.length) ++ ", 0fr)" )
            , ( "border", "1px solid transparent" )
            , ( "align-items", "start" )
            , ( "border-color"
              , if ctx.pointer == [ FHere ] then
                    "#ddd"

                else if (ctx.pointer |> List.tail |> Maybe.andThen List.head) == Just FHere then
                    "#666"

                else if ctx.cursor == [ FHere ] then
                    "#ff9"

                else if (ctx.cursor |> List.tail |> Maybe.andThen List.head) == Just FHere then
                    "#764"

                else
                    "#444"
              )

            --, ( "grid-gap", "10px" )
            --, ( "grid-auto-rows", "minmax(100px, auto)" )
            ]
        )
        (grid
            |> List.concatMap
                (\items ->
                    items
                        |> List.map
                            (\( f, t ) ->
                                div [ style "display" "flex" ] [ vTree (stepCtx f ctx) t ]
                            )
                )
        )


vNode : Ctx -> Dir4 -> List ( Focus, Tree ) -> Html Msg
vNode ctx dir syns =
    span
        (List.map (\( a, b ) -> style a b)
            [ ( "border", "1px solid transparent" )
            , ( "border-color"
              , if ctx.pointer == [ FHere ] then
                    "#ddd"

                else if (ctx.pointer |> List.tail |> Maybe.andThen List.head) == Just FHere then
                    "#666"

                else if ctx.cursor == [ FHere ] then
                    "#ff9"

                else if (ctx.cursor |> List.tail |> Maybe.andThen List.head) == Just FHere then
                    "#764"

                else
                    "#444"
              )
            , ( "display", "flex" )
            , ( "flex-direction", flexDir dir )
            , ( "padding", "1px" )
            , ( "align-items", "baseline" )
            , ( "border-radius", "3px" )
            , ( "margin", "1px" )
            ]
            ++ [ stopPropagationOn "mousemove" (Json.Decode.succeed ( Pointer (ctx.here ++ [ FHere ]), True ))
               , stopPropagationOn "click" (Json.Decode.succeed ( Cursor (ctx.here ++ [ FHere ]), True ))
               ]
        )
        (syns
            |> List.map
                (\( f, t ) ->
                    vTree
                        (if f == FHere then
                            ctx

                         else
                            stepCtx f ctx
                        )
                        t
                )
        )


vTerm : Ctx -> Term -> Html Msg
vTerm ctx term =
    span
        (List.map (\( a, b ) -> style a b)
            [ ( "border", "1px solid transparent" )
            , ( "display", "flex" )
            , ( "padding", "1px" )
            , ( "align-items", "baseline" )
            , ( "border-radius", "3px" )
            , ( "cursor", "initial" )
            , ( "border-color"
              , if ctx.pointer == [ FHere ] then
                    "#ddd"

                else if ctx.cursor == [ FHere ] then
                    "#ff9"

                else
                    "transparent"
              )
            ]
            ++ [ stopPropagationOn "mousemove" (Json.Decode.succeed ( Pointer (ctx.here ++ [ FHere ]), True ))
               , stopPropagationOn "click" (Json.Decode.succeed ( Cursor (ctx.here ++ [ FHere ]), True ))
               ]
        )
    <|
        case term of
            TermTypId ( m, t ) ->
                [ text t ]

            TermTypVarName name ->
                [ span [ style "color" "#adf" ] [ text name ] ]

            TermTypField name ->
                [ text name ]


vToken : String -> Html Msg
vToken str =
    span [ style "display" "flex", style "color" "#666", style "padding" "2px" ] [ text str ]
