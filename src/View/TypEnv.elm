module View.TypEnv exposing (viewTypEnv, viewTypEnvItem)

import Dict
import Fi exposing (fi)
import Html exposing (..)
import Structure.Typ exposing (..)
import Structure.TypEnv exposing (..)


viewTypEnv : TypEnv -> Html msg
viewTypEnv env =
    div []
        ([ h4 [] [ text "typs" ] ] ++ (env |> Dict.toList |> List.map viewTypEnvItem))


viewTypEnvItem : ( TypId, ( List String, Bool ) ) -> Html msg
viewTypEnvItem ( ( m, n ), ( args, isDef ) ) =
    div []
        [ text (fi isDef "type" "alias")
        , text " "
        , text <| m
        , text "."
        , text <| n
        , text " "
        , text <| (args |> List.map (\a -> a ++ " ") |> String.join "")
        ]
